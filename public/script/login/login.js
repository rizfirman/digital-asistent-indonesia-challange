const submit = document.getElementById('submit');
const input = document.getElementById('password');

const test = function submitFunction() {
  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;
  const msg = JSON.stringify({ username, password });
  async function postData() {
    const response = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: msg
    }
    return response.json();
  }

  postData().then((data) => {
    console.log(data.token)
    alert(data.message);
    window.location.assign(data.path);
  });
};

submit.addEventListener('click', test);

input.addEventListener('keypress', (event) => {
  if (event.key === 'Enter') {
    submit.click();
  }
});