#Technical Test WebDev

Slice this UI into web code https://www.figma.com/file/wkoOVZfT8aFcovqE77QK9W/Figma-Admin-Dashboard-UI-Kit-(Community)?node-id=0%3A1

create API/Backend for the table in tickets feature

1. user can login into admin page
2. user can view all tickets
3. user can update ticket status to closed & pending
4. closed ticket will be off from all tickets menu

----------------------------------------------------------------------------------
This is how to use this simple application:
note: I use PostgreSQL to store data

1. Please migrate the data in the Model and Seeder folder to PostgreSQL first
2. Login using admin data in PostgreSQL
3. If you want to create a customer ticket, visit http: // localhost: 3000 / createticket
4. If you want to delete a ticket, please press the "close ticket" button next to the table row on the http: // localhost: 3000 / dashboard / ticket page
5. To view the contents of the message, please press the ticket details in the table on the ticket page
