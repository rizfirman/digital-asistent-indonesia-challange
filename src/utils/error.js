const genericErrorHandler = (error, req, res, next) => {
    console.error('Error ', error)
    res.send({
      status: error.status,
      statusText: error.message,
      data: {}
    })
    next()
  }
  
  class StandardError extends Error {
    constructor(message, status, additionalInfo, stack) {
      super(message)
      this.status = status
      this.stack = stack
      console.error('Error in StandardError', message, additionalInfo)
    }
  }
  
  export { genericErrorHandler, StandardError }
  