import { StandardError } from '../../utils/error'

import { Ticket} from '../../models'


const getTickets = async () => {
    try {
    
       const ticket = await Ticket.findAll({
           
       })
        return {
            status: 200,
            statusText: 'Success',
            data: ticket
        }
    } catch (err) {
        throw new StandardError(err.message, 400)
    }
}

const createTicket = async (params) => {
    try{

        const {
            costumer_name, email, title, priority, problem
          } = params;

        const tickets = await Ticket.create({
            costumer_name, 
            email, 
            title, 
            priority, 
            problem
        })

        return {
            status: 200,
            statusText: 'Success',
            data: tickets
        }
    } catch (err) {
        throw new StandardError (err.message, 400)
    }

}

const deleteTickets = async (params) => {
    try {

        const {id} = params
        const deleted = await Ticket.findOne({
           where: {id}
        })
       
        return {
            status: 200,
            statusText: 'Success',
            data: deleted
        }
    } catch (err) {
        throw new StandardError(err.message, 400)
    }
}

export { getTickets, createTicket, deleteTickets}