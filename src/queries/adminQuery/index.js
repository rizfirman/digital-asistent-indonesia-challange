import { StandardError } from '../../utils/error'
import {Admin} from '../../models'




const getLogin = async (params) => {
    try {
    const {username: name , password: userPass} = params
    const admin = await Admin.findOne({
      where: {
        username: name,
        password: userPass
      }
    })
  
   
      return {
        status: 201,
        statusText: 'Success',
        data: admin
      }
    } catch (err) {
      throw new StandardError(err.message, err.status || 500)
    }
  }

  export { getLogin }