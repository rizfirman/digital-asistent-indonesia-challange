import { getTickets, createTicket, deleteTickets } from '../../queries/ticketQuery'


export default class TicketController {
   
    static getTicketPage = async (req, res, next) => {
        try {
            
            const result = await getTickets(req.query)

            const user = req.session.username
            
            if(!user) {
                return res.send('Please, login first')
            }
            
            return res.status(result.status).render('../src/views/ticketpage', 
            {
                title: 'ticket', 
                login: true, 
                username: user,
                ticket: result.data
                
            }) 

        } catch (err) {
            return next(err)
        }
    }

    static getCreateTicketPage = async (req, res, next) => {
        try {
            
            
            return res.status(200).render('../src/views/createticket', 
            {title: 'createticket'}) 

        } catch (err) {
            return next(err)
        }
    } 
    
    static createCostumerTicket = async (req, res, next) => {
        try {

             const result=await createTicket(req.body)

            return res.status(result.status).send("Ticket was created")

        } catch (err) {
            return next(err)
        }
    } 

    static deletedCostumerTicket = async (req, res, next) => {
        try {

            const result = await deleteTickets(req.params)
           
            if (result.data) {
                result.data.destroy({})
            }
            
            return res.redirect(301, '/dashboard/ticket')

        } catch (err) {
            return next(err)
        }
    } 

    
}