
import { getLogin } from '../../queries/adminQuery'

let login = false;

export default class AdminController {

    static getLoginPage = async (req, res, next) => {
        try {

            return await res.status(200).render('../src/views/adminlogin', 
            {title: 'login', login})
        } catch (err) {
            return next(err)
        }
    } 

    static getDashbordPage = async (req, res, next) => {
        try {

            return await res.status(200).render('../src/views/dashboard', 
            {title: 'login', login, username: req.session.username})
        } catch (err) {
            return next(err)
        }
    } 

    static getLoginAdmin= async (req, res, next) => {
        try {


            const result = await getLogin(req.body)

            if (result.data){
                req.session.username = req.body.username
                login = true
            }

            if(!result.data) {
                return res.status(400).send('your not admin')
            }

            return res.redirect(301, '/dashboard' )


        } catch (err) {
            return next(err)
        }
    } 

    
    
}