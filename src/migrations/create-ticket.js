
module.exports =  {
 up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Ticket', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        unique: true,
      },
      costumer_name: { 
        type: Sequelize.STRING,
        allowNull: false,        
      },
      email: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      title: { 
        type: Sequelize.STRING,
        allowNull: false,
      },
      priority: {
        type: Sequelize.ENUM,
        values: ['LOW', 'NORMAL', 'HIGH'],
        allowNull: false,
      },
      problem: {
          type: Sequelize.STRING,
          allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    },
down: async (queryInterface, Sequelize) => {
await queryInterface.dropTable('Ticket')
await queryInterface.sequelize.query("DROP TYPE \"enum_Ticket_priorty\";")
}
};
