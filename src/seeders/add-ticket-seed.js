module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.bulkInsert('Ticket', [
            {
                id: 'c4cb353b-0f1e-455b-b634-562a6d39bc90',
                costumer_name: 'Rizki Firmansyah',
                email: 'rizfirman@gmail.com',
                title: 'gangguan',
                priority: 'HIGH',
                problem: 'blablabla',
                createdAt: '2021-01-25 09:00:00.000+07',
                updatedAt: '2021-01-27 09:00:00.000+07'
            },
            {
                id: '61dfc472-f3c2-4c99-a40f-b190cef9a8b6',
                costumer_name: 'Herman',
                email: 'Herman@gmail.com',
                title: 'gangguan',
                priority: 'NORMAL',
                problem: 'blablabla',
                createdAt: '2021-01-25 09:00:00.000+07',
                updatedAt: '2021-01-27 09:00:00.000+07'
            },
            {
                id: '17d8679f-b7ad-429b-9a87-d8a5925c4e32',
                costumer_name: 'Roni',
                email: 'Roni@gmail.com',
                title: 'gangguan',
                priority: 'LOW',
                problem: 'blablabla',
                createdAt: '2021-01-25 09:00:00.000+07',
                updatedAt: '2021-01-27 09:00:00.000+07'
            }], {});
    },
    down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Ticket', null, {});
    },
}