import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class Ticket extends Model {
   
  }
  Ticket.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        unique: true,
      },
      costumer_name: { 
        type: DataTypes.STRING,
        allowNull: false,        
        },
      email: {
        type: DataTypes.STRING,
        allowNull:false,
      },
      title: { 
        type: DataTypes.STRING,
        allowNull: false,
    },
      priority: {
        type: DataTypes.ENUM,
        values: ['LOW', 'NORMAL', 'HIGH'],
        allowNull: false,
      },
      problem: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'Ticket',
      timestamps: true,
    },
  );
  return Ticket;
};
