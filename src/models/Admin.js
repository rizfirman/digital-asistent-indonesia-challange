import { Model } from 'sequelize'

module.exports = (sequelize, DataTypes) => {
  class Admin extends Model {
    
  }
  Admin.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        unique: true,
      },
      username: { 
       type: DataTypes.STRING,
       allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,

      },
    },
    {
      sequelize,
      modelName: 'Admin',
      timestamps: false,
    },
  );
  return Admin;
};
