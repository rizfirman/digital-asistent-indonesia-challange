const ROUTES = {
    CREATETICKET: '/createticket',
    LOGIN: '/',
    DASHBOARD: '/dashboard',
    TICKET: '/dashboard/ticket',
    DELETETICKET:'/dashboard/:id'
}

export default ROUTES