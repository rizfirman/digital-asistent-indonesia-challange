import { Router } from 'express'
import ROUTES from './routes'
import TicketController from '../controllers/ticketController'
import AdminController from '../controllers/adminController'


const routes = Router()

routes.get(ROUTES.LOGIN, AdminController.getLoginPage)
routes.get(ROUTES.DASHBOARD, AdminController.getDashbordPage)
routes.get(ROUTES.TICKET, TicketController.getTicketPage )
routes.get(ROUTES.CREATETICKET, TicketController.getCreateTicketPage)
routes.get(ROUTES.DELETETICKET, TicketController.deletedCostumerTicket)

routes.post(ROUTES.LOGIN, AdminController.getLoginAdmin)
routes.post(ROUTES.CREATETICKET, TicketController.createCostumerTicket)





export default routes