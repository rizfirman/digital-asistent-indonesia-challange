import express from 'express'
import logger from 'morgan'
import session from 'express-session';
import db from './models'
import routes from './routes'
import { genericErrorHandler } from './utils/error'


require('dotenv').config()

const app = express()
const PORT = process.env.PORT || 3100

db.sequelize
  .authenticate()
  .then(() =>
    console.log(
      `Database ${process.env.DB_DATABASE} connected on port:${process.env.DB_PORT}`
    )
  )
  .catch((err) => console.log(`Database connection error: ${err}`))

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.get('/', (req, res) => {
  res.status(200).render('../src/views/adminlogin')
})

app.use(session({
  name: process.env.SESSION_NAME,
  resave: false,
  saveUninitialized: false,
  secret: process.env.SESSION_SECRET,
  cookie: {
    maxAge: 2 * 60 * 60 * 1000, // 2 hour in milliseconds
    sameSite: true,
    // For development secure = false
    secure: false,
  },
}));

app.use('/', routes)



app.use((req, res, next) => {
  res.status(404).send('<h1>404</h1>')
})
app.use(genericErrorHandler)



app.listen(PORT, () =>
  console.log(`Server running on http://localhost: ${PORT}`)
)

